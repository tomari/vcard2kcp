#!/usr/bin/perl
# Convert vCard file originating from Apple MacOS X Addres Book
# to format compatible with KCP (BREW) dumbphones
#
# put output in /PRIVATE/AU/DF/D_PI/PIA_0000.VCF
# 
use strict;
use warnings;
use Text::Iconv;
use Lingua::JA::Moji 'romaji2kana','kana2hw';

sub create_yomigana {
  my $nfield=shift;
  my @alphabets;
  $nfield =~ s/^.+://;
  foreach(split(//, $nfield)) {
    if(/[a-zA-Z]/) {
      push @alphabets,$_;
    }
  }
  my $romanexpr=join('',@alphabets);
  my $kana=romaji2kana($romanexpr);
  return kana2hw($kana);
}

sub format_vcard {
  my $sourcepath=shift;
  my $converter=Text::Iconv->new("utf-8","windows-31j");
  open(my $src, '<', $sourcepath) or die $!;
  my $yomigana;
  my $yomigana_hakken;
  while(<$src>) {
    s/^item.\.//;
    if(/^BEGIN:VCARD/) {
      $yomigana=undef;
      $yomigana_hakken=undef;
    } elsif(/^N[;:]/) {
      s/^N.*:/N;CHARSET=SHIFT_JIS:/;
      $yomigana=create_yomigana($_);
    } elsif(/^FN[;:]/) {
      s/^FN.*:/FN;CHARSET=SHIFT_JIS:/;
    } elsif(/^CATEGORIES[;;]/) {
      next;
    } elsif(/^PRODID[;:]/) {
      next;
    } elsif(/^X-ABUID[;:]/) {
      next;
    } elsif(/^CATEGORIES[;:]/) {
      next;
    } elsif(/^SORT-STRING[;:]/) {
      $yomigana_hakken=1;
    } elsif(/^END:VCARD/) {
      if(!defined($yomigana_hakken) && defined($yomigana)) {
	print 'SORT-STRING;CHARSET=SHIFT_JIS:';
	print $converter->convert($yomigana);
	print "\n";
      }
    }
    print $converter->convert($_);
  }
  close $src;
}

if(@ARGV>0) {
    format_vcard($ARGV[0]);
} else {
    print STDERR "usage: formatvcard.pl vcards.vcd > /mnt/sdcard/PRIVATE/AU/DF/D_PI/PIA_0000.VCF\n"
}

